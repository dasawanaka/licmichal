package michal.daniel.lic.geometry;

import static java.lang.Double.max;
import static java.lang.Double.min;

/**
 * see http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Algorithms {

    /**
     *
     * @param p1 Punkt 1
     * @param p2 Punkt 2
     * @param p3 Punkt 3
     * @return True jeśli punkt p2 leży pomiędzy p1 a p3. False w przeciwnym
     * wypadku.
     */
    public boolean onSegment(Point p1, Point p2, Point p3) {
        return p2.getX() <= max(p1.getX(), p3.getX()) && p2.getX() >= min(p1.getX(), p3.getX())
                && p2.getY() <= max(p1.getY(), p3.getY()) && p2.getY() >= min(p1.getY(), p3.getY());
    }

    /**
     * Zwraca orientacje punktu p2 względem prostej utworzonej z p1 i p3.
     * Implementacja za pomocą
     * https://www.geeksforgeeks.org/orientation-3-ordered-points/
     *
     * @param p1 Punkt 1
     * @param p2 Punkt 2
     * @param p3 Punkt 3
     * @return 0 - p, q and r are colinear, 1 - Clockwise, 2 - Counterclockwise
     */
    public int orientation(Point p1, Point p2, Point p3) {
        //
        double val = (p2.getY() - p1.getY()) * (p3.getX() - p2.getX())
                - (p2.getX() - p1.getX()) * (p3.getY() - p2.getY());

        if (val == 0) {
            return 0;  // colinear
        }
        return (val > 0) ? 1 : 2; // clock or counterclock wise
    }

    /**
     * Sprawdza przecięcie dwóch prostych (p1, q1) oraz (p2, q2).
     */
    public boolean doIntersect(Point p1, Point q1, Point p2, Point q2) {

        int o1 = orientation(p1, q1, p2);
        int o2 = orientation(p1, q1, q2);
        int o3 = orientation(p2, q2, p1);
        int o4 = orientation(p2, q2, q1);

        if (o1 != o2 && o3 != o4) {
            return true;
        }
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1
        if (o1 == 0 && onSegment(p1, p2, q1)) {
            return true;
        }
        // p1, q1 and q2 are colinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(p1, q2, q1)) {
            return true;
        }
        // p2, q2 and p1 are colinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(p2, p1, q2)) {
            return true;
        }
        // p2, q2 and q1 are colinear and q1 lies on segment p2q2
        if (o4 == 0 && onSegment(p2, q1, q2)) {
            return true;
        }

        return false; // Doesn't fall in any of the above cases
    }

    /**
     * Zwraca długość pomiędzy punktami z parametrów.
     *
     * @param p Punkt 1
     * @param q Punkt 2
     * @return Odległość typu double.
     */
    public double distance(Point p, Point q) {
        return Math.sqrt((p.getX() - q.getX()) * (p.getX() - q.getX())
                + (p.getY() - q.getY()) * (p.getY() - q.getY()));
    }

    /**
     * Zwraca długość euklidesową pomiędzy punktami z parametrów.
     *
     * @param a Punkt a
     * @param b Punkt b
     * @return Odległość typu double.
     */
    public double euclideanDistance(Point a, Point b) {

        double deltaX = a.getY() - b.getY();// ycoord - other.ycoord;
        double deltaY = a.getX() - b.getX();//xcoord - other.xcoord;
        double result = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        return result;

    }

    /**
     * Sprawdza przecięcie pomiędzy dwoma odcinkami z parametrów.
     *
     * @see Segment
     * @param a Odcinek a
     * @param b Odcinek b
     */
    public boolean doIntersect(Segment a, Segment b) {
        return doIntersect(a.getP(), a.getR(), b.getP(), b.getR());
    }
}
