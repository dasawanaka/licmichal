package michal.daniel.lic.geometry;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Reprezentacja odcinka.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Segment {

    private static final Algorithms ALGORITHMS = new Algorithms();
    private final Point p;
    private final Point r;
    private final double weight;

    public Segment(Point p, Point r) {
        this.p = p;
        this.r = r;
        this.weight = ALGORITHMS.euclideanDistance(p, r);
    }

    public Point getP() {
        return p;
    }

    public Point getR() {
        return r;
    }

    public double getWeight() {
        return weight;
    }

    /**
     * Funkcja rysująca w obszarze g2d odcinek.
     *
     * @param g2d Komponent grafiki 2D.
     * @param col Kolor odcinka.
     * @param thickness Grubość linii.
     */
    public void draw(Graphics2D g2d, Color col, int thickness) {
        g2d.setColor(col);
        g2d.setStroke(new BasicStroke(thickness));
        g2d.draw(new Line2D.Float(p.getX(), p.getY(), r.getX(), r.getY()));
    }

    /**
     * 
     * @return Lista punktów odcinka.
     */
    public ArrayList<Point> getPoints() {
        ArrayList<Point> result = new ArrayList<>();
        result.add(p);
        result.add(r);
        return result;
    }

}
