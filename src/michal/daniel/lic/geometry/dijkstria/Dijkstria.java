package michal.daniel.lic.geometry.dijkstria;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import michal.daniel.lic.geometry.Algorithms;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;
import michal.daniel.lic.geometry.graph.VisibilityGraph;

/**
 * Dijkstia alghoritm. Implement using
 * http://algorytmy.ency.pl/tutorial/algorytm_dijkstry and T.H. Cormen, Ch.E.
 * Leiserson, R.L. Rivest, C. Stein, Wprowadzenie do algorytmów, Wydawnictwo
 * Naukowe PWN, Warszawa, 2012.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Dijkstria {

    private Map<Point, ArrayList<Point>> visibilityGraph;
    private Map<Point, DijkstriaNode> dijkstriaMap;
    private static final Algorithms ALGORITHMS = new Algorithms();
    private ArrayList<Point> shortestPath;
    private ArrayList<Point> visited;

    public Dijkstria(VisibilityGraph vg) {
        this.visibilityGraph = vg.getGraph();
        dijkstriaMap = new HashMap<>();
        visited = new ArrayList<>();
        Set<Point> keySet = visibilityGraph.keySet();
        keySet.forEach((point) -> {
            DijkstriaNode dn = new DijkstriaNode();
            dn.setDistanceVertex(-1);
            dn.setPreviousVertex(null);
            dijkstriaMap.put(point, dn);
        });
    }

    /**
     * Obliczanie najkrótszej trasy między punktami podamymi w parametrach.
     *
     * @param s Start Point.
     * @param e End Point.
     */
    public void computeShortestPath(Point s, Point e) {
        DijkstriaNode dn = new DijkstriaNode(0, null);
        dijkstriaMap.put(s, dn);
        computeShortestPathPriv(s);
        shortestPath = getPath(e);
    }

    private void distancCorrect(Segment s) {
        Point s1 = s.getP();
        Point s2 = s.getR();
        DijkstriaNode dn = dijkstriaMap.get(s2);
        double d = dn.getDistanceVertex();
        double pd = dijkstriaMap.get(s1).getDistanceVertex();

        if (d == -1 || (pd + s.getWeight()) < d) {
            dijkstriaMap.get(s2).setDistanceVertex((pd + s.getWeight()));
            dijkstriaMap.get(s2).setPreviousVertex(s1);
        }
    }

    private void computeShortestPathPriv(Point s) {
        if (!visited.contains(s)) {

            ArrayList<Point> vp = visibilityGraph.get(s);

            Point minPoint = null;
            double minWeight = Double.MAX_VALUE;
            double sDistance = dijkstriaMap.get(s).getDistanceVertex();

            vp.forEach((p) -> {
                distancCorrect(new Segment(s, p));
            });
            visited.add(s);
            vp.forEach((p) -> {
                computeShortestPathPriv(p);
            });

            for (Point p : vp) {
                Segment seg = new Segment(s, p);
                distancCorrect(seg);

                if (seg.getWeight() + sDistance < minWeight && seg.getWeight() > 0) {
                    minPoint = p;
                    minWeight = seg.getWeight() + sDistance;
                }
            }
            computeShortestPathPriv(minPoint);
        }
    }

    private ArrayList<Point> getPath(Point end) {
        ArrayList<Point> result = new ArrayList<>();
        Point temp = end;

        while (temp != null) {
            result.add(temp);
            temp = dijkstriaMap.get(temp).getPreviousVertex();
        }

        return result;
    }

    /**
     *
     * @return Lista zawierająca punkty z najkrótszej wygenerowanej ścieżki.
     */
    public ArrayList<Point> getShortestPath() {
        return shortestPath;
    }

    /**
     * Funkcja która na obszarze gfx rysuje najkrótszą ściezkę wcześniej
     * wygenerowaną.
     *
     * @param gfx Komponent grafiki 2d jPaanela.
     */
    public void draw(Graphics2D gfx) {
        Point p = shortestPath.get(0);
        Point c;
        Segment s;
        for (int i = 1; i < shortestPath.size(); i++) {
            c = shortestPath.get(i);
            s = new Segment(p, c);
            s.draw(gfx, Color.red, 3);
            p = c;
        }
    }
}
