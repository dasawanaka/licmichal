package michal.daniel.lic.geometry.dijkstria;

import michal.daniel.lic.geometry.Point;

/**
 * Węzeł wykorzystywany w algorytmie Dijkstri.
 *
 * @see Dijkstria
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class DijkstriaNode {

    private double distanceVertex;
    private Point previousVertex;

    public DijkstriaNode() {
    }

    public DijkstriaNode(float distanceVertex, Point previousVertex) {
        this.distanceVertex = distanceVertex;
        this.previousVertex = previousVertex;
    }

    public double getDistanceVertex() {
        return distanceVertex;
    }

    public void setDistanceVertex(double distanceVertex) {
        this.distanceVertex = distanceVertex;
    }

    public Point getPreviousVertex() {
        return previousVertex;
    }

    public void setPreviousVertex(Point previousVertex) {
        this.previousVertex = previousVertex;
    }

}
