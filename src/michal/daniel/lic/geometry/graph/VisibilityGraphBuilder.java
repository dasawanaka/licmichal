package michal.daniel.lic.geometry.graph;

import michal.daniel.lic.geometry.Segment;
import java.util.ArrayList;
import java.util.List;
import michal.daniel.lic.geometry.Algorithms;
import michal.daniel.lic.geometry.Point;

/**
 *
 * @author Michał‚ Daniel (michal.mateusz.daniel@gmail.com)
 */
public class VisibilityGraphBuilder {

    public static final Algorithms ALGORITHMS = new Algorithms();

    public VisibilityGraphBuilder() {
    }

    /**
     * Generuje graf widoczności na podstawie przeszkód.
     *
     * @param obstacles Lista przeszkód.
     * @see Segment
     * @see VisibilityGraph
     * @return Struktura zawierająca graf widoczności.
     */
    public VisibilityGraph makeVisibilityGraph(List<Segment> obstacles) {
        VisibilityGraph visibilityGraph = new VisibilityGraph();
        obstacles.forEach((Segment obstacle) -> {
            obstacle.getPoints().forEach((point) -> {
                ArrayList<Point> visPoints = visiblePoints(point, obstacles);
                visibilityGraph.addNode(point, visPoints);
            });
        });
        return visibilityGraph;
    }

    private ArrayList<Point> visiblePoints(Point p, List<Segment> obstacles) {
        ArrayList<Point> resulList = new ArrayList<>();
        obstacles.forEach((Segment obstacle) -> {
            obstacle.getPoints().stream()
                    .filter((point) -> (isVisible(p, point, obstacles)))
                    .forEachOrdered((point) -> {
                        resulList.add(point);
                    });
        });
        return resulList;
    }

    private boolean isVisible(Point p, Point r, List<Segment> obstacles) {
        /*if (p.equals(r)) {
            return true;
        }*/
        Segment tmp = new Segment(p, r);
        for (Segment seg : obstacles) {
            if (!(seg.getP().getObstacleId() == tmp.getP().getObstacleId() || seg.getP().getObstacleId() == tmp.getR().getObstacleId())) {
                if (ALGORITHMS.doIntersect(tmp, seg)) {
                    return false;
                }
            }
        }
        return true;
    }
}
