package michal.daniel.lic.geometry.graph;

import java.awt.Color;
import michal.daniel.lic.geometry.Segment;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import michal.daniel.lic.geometry.Point;

/**
 * Klasa reprezentująca graf widoczności.
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class VisibilityGraph {

    private final Map<Point, ArrayList<Point>> graph;

    public VisibilityGraph() {
        graph = new HashMap<>();
    }

    public void addNode(Point point, ArrayList<Point> visiblePoints) {
        graph.put(point, visiblePoints);
    }

    /**
     * Zwracamy graf widoczności w postaci mapy o kluczu Point i zawartością
     * typu ArrayList typu Point.
     *
     * @see Point
     * @see Map
     * @return Graf w postaci mapy.
     */
    public Map<Point, ArrayList<Point>> getGraph() {
        return graph;
    }

    /**
     * Funkcja która na obszarze gfx rysuje graf widoczności wcześniej
     * wygenerowany.
     *
     * @param gfx Komponent grafiki 2d jPaanela.
     */
    public void draw(Graphics2D gfx) {
        Set<Point> keySet = graph.keySet();
        keySet.forEach((Point point) -> {
            ArrayList<Point> visPoints = graph.get(point);
            visPoints.stream()
                    .map((vPoint) -> new Segment(point, vPoint))
                    .forEachOrdered((sg) -> {
                        sg.draw(gfx, new Color(110, 197, 229), 1);
                    });
        });
    }

}
