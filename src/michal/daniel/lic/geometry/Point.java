package michal.daniel.lic.geometry;

import michal.daniel.lic.geometry.graph.VisibilityGraph;

/**
 * Własna implementacja punktu. Oprócz współrzędnych zawiera id przeszkody.
 * @see Segment
 * @see VisibilityGraph
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Point {

    private float x;
    private float y;
    private int obstacleId;

    public Point(float x, float y, int obstacleId) {
        this.x = x;
        this.y = y;
        this.obstacleId = obstacleId;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getObstacleId() {
        return obstacleId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Point)) {
            return false;
        }

        Point p = (Point) obj;
        
        return x == p.x && y == p.y;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Float.floatToIntBits(this.x);
        hash = 47 * hash + Float.floatToIntBits(this.y);
        return hash;
    }
    

}
