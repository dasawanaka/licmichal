package michal.daniel.lic.view.animation;

import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import michal.daniel.lic.geometry.Point;

/**
 * Klasa reprezentująca ścieżkę wykorzystywaną podczas animacji ruchu robota.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class PathFollower {

    private final List<Point2D> points;
    private Shape path;
    private double pathLength = -1;
    private double alpha = 0;

    public PathFollower() {
        this.points = new ArrayList<>();
    }

    public PathFollower(List<Point> pointsL) {
        this.points = new ArrayList<>();
        pointsL.stream()
                .map((p) -> new Point2D.Double(p.getX(), p.getY()))
                .forEachOrdered((p2d) -> {
                    addPoint(p2d);
                });

    }

    /**
     * Dodanie punktu do ścieżki.
     *
     * @param p Punkt
     */
    public void addPoint(Point2D p) {
        points.add(0, new Point2D.Double(p.getX(), p.getY()));
        path = null;
        pathLength = -1;
    }

    /**
     * Ustawimy postęp ścieżki.
     *
     * @param alpha
     */
    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    /**
     * Zwraca aktualny punkt na ścieżce przy animacji biorąc pod uwagę postęp
     * drogi (alpha).
     *
     * @return Point
     */
    public Point2D getCurrentPoint() {
        return computePoint(alpha);
    }

    /**
     * Zwraca pełą ścieżkę.
     */
    public Shape getPath() {
        if (path == null) {
            path = createPath();
        }
        return path;
    }

    private Shape createPath() {
        Path2D Path2D = new Path2D.Double();
        for (int i = 0; i < points.size(); i++) {
            Point2D p = points.get(i);
            double x = p.getX();
            double y = p.getY();
            if (i == 0) {
                Path2D.moveTo(x, y);
            } else {
                Path2D.lineTo(x, y);
            }
        }
        return Path2D;
    }

    private double computePathLength() {
        double pathLength = 0;
        for (int i = 0; i < points.size() - 1; i++) {
            Point2D p0 = points.get(i);
            Point2D p1 = points.get(i + 1);
            pathLength += p0.distance(p1);
        }
        return pathLength;
    }

    private Point2D computePoint(double alpha) {
        if (pathLength < 0) {
            pathLength = computePathLength();
        }
        double alphaPosition = alpha * pathLength;
        double accumulatedLength = 0;
        for (int i = 0; i < points.size() - 1; i++) {
            Point2D p0 = points.get(i);
            Point2D p1 = points.get(i + 1);
            double distance = p0.distance(p1);
            double nextLength = accumulatedLength + distance;
            if (nextLength >= alphaPosition) {
                double localAlpha
                        = (alphaPosition - accumulatedLength) / distance;
                double x = p0.getX() + localAlpha * (p1.getX() - p0.getX());
                double y = p0.getY() + localAlpha * (p1.getY() - p0.getY());
                return new Point2D.Double(x, y);
            }
            accumulatedLength = nextLength;
        }
        Point2D p = points.get(points.size() - 1);
        return new Point2D.Double(p.getX(), p.getY());
    }

}
