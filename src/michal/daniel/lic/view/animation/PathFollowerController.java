package michal.daniel.lic.view.animation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import michal.daniel.lic.view.frame.RepaintingPanel;

/**
 * Kontroler animacji po ścieżke.
 *
 * @see PathFollower
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class PathFollowerController {

    private int speed = 0;
    private PathFollower pathFollower;
    private RepaintingPanel pathFollowerPanel;

    private final Timer timer = new Timer(50, new ActionListener() {
        private double alpha = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            alpha += speed / 500.0;
            alpha %= 1.0;
            while (alpha < -1.0) {
                alpha += 1.0;
            }
            pathFollower.setAlpha(alpha < 0 ? -alpha : alpha);
            pathFollowerPanel.repaint();
        }
    });

    public PathFollowerController(PathFollower pathFollower,
            RepaintingPanel pathFollowerPanel) {
        this.pathFollower = pathFollower;
        this.pathFollowerPanel = pathFollowerPanel;
    }

    /**
     * Wystartowanie bądź zatrzymamie animacji.
     *
     * @param running Wartość logiczna.
     */
    public void setRunning(boolean running) {
        if (running) {
            timer.start();
        } else {
            timer.stop();
        }
    }

    /**
     * Ustawienie prędkości animacji.
     * @param speed Prędkość.
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
