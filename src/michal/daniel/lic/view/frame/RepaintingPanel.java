package michal.daniel.lic.view.frame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.JPanel;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;
import michal.daniel.lic.geometry.dijkstria.Dijkstria;
import michal.daniel.lic.geometry.graph.VisibilityGraph;
import michal.daniel.lic.geometry.graph.VisibilityGraphBuilder;
import michal.daniel.lic.view.animation.PathFollower;
import michal.daniel.lic.view.animation.PathFollowerController;

/**
 * Samo odmalowywujący się panel. Rozrzerza podstawowy panel z javy. Wszytskie
 * elememnty klasy będą odmalowywać się automatycznie/
 *
 * @see JPanel
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class RepaintingPanel extends JPanel {

    private VisibilityGraph visibilityGraph; // pattern visibility graph
    private ArrayList<Segment> obstacle;
    private Dijkstria dijkstria;
    private Point start, end;
    private boolean vgDraw;

    //size point and robot
    private final int pointSizeX = 10;
    private final int pointSizeY = 10;
    private final int robotSizeX = 10;
    private final int robotSizeY = 10;

    //animation variables
    private final int speedAnimation = 5;
    private PathFollower pathFollower;
    private PathFollowerController pathFollowerController;

    /**
     * Ustawiamy przeszkody, które mają się odmalowywać podczas działania
     * programu.
     *
     * @see Segment
     * @param obstacle Lista przeszkód.
     */
    public void setObstacle(ArrayList<Segment> obstacle) {
        this.obstacle = obstacle;
    }

    /**
     * Ustawienie punktu początkowego dla szukanej ścieżki.
     *
     * @see Point
     * @param start Punkt startu.
     */
    public void setStart(Point start) {
        this.start = start;
    }

    /**
     * Ustawienie punktu końcowego dla szukanej ścieżki.
     *
     * @see Point
     * @param end Punkt końca.
     */
    public void setEnd(Point end) {
        this.end = end;
    }

    /**
     * Ustawiamy czy chcemy aby graf widoczności był wyświetlany.
     *
     * @param vgDraw
     */
    public void setVgDraw(boolean vgDraw) {
        this.vgDraw = vgDraw;
    }

    /**
     *
     * @return Czy Graf widoczności ma być wyświetlany.
     */
    public boolean isVgDraw() {
        return vgDraw;
    }

    /**
     * Uruchomienie bądź zatrzymanie animacji robota.
     *
     * @param b
     */
    public void setAnimationRunnig(boolean b) {
        pathFollowerController.setRunning(b);
    }

    /**
     * Ponowne obliczenie grafu widoczności, najkrótszej ścieżki oraz
     * zaktualizowanie ścieżki dla animacji. Trzeba użyć po aktualizacjach
     * danych w panelu np. zmiana punktu początkowego bądź końcowego, lub po
     * aktualizacji przeszkód.
     */
    public void recalculate() {
        if (obstacle != null && start != null && end != null) {
            ArrayList<Segment> copyObstacles = new ArrayList<>();
            copyObstacles.addAll(obstacle);
            Segment s = new Segment(start, start);
            Segment e = new Segment(end, end);

            copyObstacles.add(e);
            copyObstacles.add(s);

            VisibilityGraphBuilder vgb = new VisibilityGraphBuilder();
            visibilityGraph = vgb.makeVisibilityGraph(copyObstacles);

            dijkstria = new Dijkstria(visibilityGraph);
            dijkstria.computeShortestPath(start, end);

            //for animation
            ArrayList<Point> shortestPath = dijkstria.getShortestPath();
            pathFollower = new PathFollower(shortestPath);

            pathFollowerController = new PathFollowerController(pathFollower, this);
            pathFollowerController.setSpeed(speedAnimation);
            pathFollowerController.setRunning(false);
        }
    }

    /**
     * Funkcja, która odmalowywuje formatkę za każdym wywołaniem repaint() w
     * programie.
     */
    @Override
    public void paint(Graphics g) {
        Graphics2D gfx = (Graphics2D) g;
        gfx.setColor(Color.white);
        gfx.fillRect(0, 0, 2000, 2000);

        gfx.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        if (obstacle != null) {
            obstacle.forEach((segment) -> {
                segment.draw(gfx, Color.MAGENTA, 3);
            });
        }
        if (visibilityGraph != null && vgDraw) {
            visibilityGraph.draw(gfx);
        }
        if (dijkstria != null) {
            dijkstria.draw(gfx);
        }
        if (start != null) {
            drawPoint(start, gfx, Color.orange);
        }
        if (end != null) {
            drawPoint(end, gfx, Color.orange);
        }
        if (pathFollower != null) {
            Point2D p = pathFollower.getCurrentPoint();
            drawRobot(p, gfx, Color.green);
        }
    }

    private void drawPoint(Point p, Graphics2D gfx, Color c) {
        gfx.setColor(Color.black);
        gfx.drawOval((int) (p.getX() - pointSizeX / 2), (int) (p.getY() - pointSizeY / 2), pointSizeX, pointSizeY);
        gfx.setColor(c);
        gfx.fillOval((int) (p.getX() - pointSizeX / 2), (int) (p.getY() - pointSizeY / 2), pointSizeX, pointSizeY);
    }

    private void drawRobot(Point2D p, Graphics2D gfx, Color c) {
        gfx.setColor(Color.black);
        gfx.drawRect((int) (p.getX() - robotSizeX / 2), (int) (p.getY() - robotSizeY / 2), robotSizeX, robotSizeY);
        gfx.setColor(c);
        gfx.fillRect((int) (p.getX() - robotSizeX / 2), (int) (p.getY() - robotSizeY / 2), robotSizeX, robotSizeY);
    }
}
