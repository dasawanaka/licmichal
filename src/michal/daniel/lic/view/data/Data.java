package michal.daniel.lic.view.data;

import java.util.ArrayList;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;

/**
 * Interfejs specyfikujący dane których używa się w programie.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public interface Data {

    /**
     * Zwraca Listę przeszkód, które są odcinkami.
     *
     * @see Segment
     * @return Lista odcinków.
     */
    public ArrayList<Segment> getObstacles();

    /**
     * Zwraca punkt który reprezentuje początek szukanej drogi.
     *
     * @see Point
     * @return Point
     */
    public Point getStartPoint();

    /**
     * Zwraca punkt który reprezentuje szukanej szukanej drogi.
     *
     * @see Point
     * @return Point
     */
    public Point getEndPoint();
}
