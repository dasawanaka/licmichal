package michal.daniel.lic.view.data;

import java.util.ArrayList;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;

/**
 * Model danych implementujący interfejs <i>Data</i>. Zawiera listę przeszkód
 * oraz punkty początkowy i końcowy. 
 *
 * @see Data
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class DataModel implements Data {

    private ArrayList<Segment> obstacles;
    private Point starPoint = null;
    private Point endPoint = null;

    @Override
    public ArrayList<Segment> getObstacles() {
        return this.obstacles;
    }

    @Override
    public Point getStartPoint() {
        return this.starPoint;
    }

    @Override
    public Point getEndPoint() {
        return this.endPoint;
    }

    public void setObstacles(ArrayList<Segment> obstacles) {
        this.obstacles = obstacles;
    }

    public void setStarPoint(Point starPoint) {
        this.starPoint = starPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
    }

}
