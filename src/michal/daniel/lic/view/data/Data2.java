package michal.daniel.lic.view.data;

import java.util.ArrayList;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;

/**
 * Dane dla mapy 2 w aplikacji.
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Data2 implements Data {

    @Override
    public ArrayList<Segment> getObstacles() {
        ArrayList<Segment> obstacle = new ArrayList<>();
        obstacle.add(new Segment(new Point(100, 150, 1), new Point(350, 500, 1)));
        obstacle.add(new Segment(new Point(50, 445, 2), new Point(250, 200, 2)));
        obstacle.add(new Segment(new Point(150, 50, 3), new Point(400, 250, 3)));
        obstacle.add(new Segment(new Point(300, 300, 4), new Point(150, 600, 4)));
        obstacle.add(new Segment(new Point(655, 100, 5), new Point(900, 250, 5)));
        obstacle.add(new Segment(new Point(700, 100, 6), new Point(605, 300, 6)));
        obstacle.add(new Segment(new Point(900, 5, 7), new Point(925, 205, 7)));
        obstacle.add(new Segment(new Point(710, 10, 8), new Point(925, 600, 8)));

        return obstacle;

    }

    @Override
    public Point getStartPoint() {
        return new Point(50, 500, 101);
    }

    @Override
    public Point getEndPoint() {
        return new Point(950, 50, 102);
    }

}
