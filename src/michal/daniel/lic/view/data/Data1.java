package michal.daniel.lic.view.data;

import java.util.ArrayList;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;

/**
 * Dane dla mapy 1 w aplikacji.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Data1 implements Data {

    @Override
    public ArrayList<Segment> getObstacles() {
        ArrayList<Segment> obstacle = new ArrayList<>();
        obstacle.add(new Segment(new Point(100, 100, 1), new Point(50, 300, 1)));
        obstacle.add(new Segment(new Point(250, 500, 2), new Point(350, 300, 2)));
        obstacle.add(new Segment(new Point(650, 5, 3), new Point(900, 350, 3)));
        obstacle.add(new Segment(new Point(850, 450, 4), new Point(650, 545, 4)));
        obstacle.add(new Segment(new Point(255, 100, 5), new Point(500, 250, 5)));
        obstacle.add(new Segment(new Point(300, 100, 6), new Point(205, 300, 6)));
        obstacle.add(new Segment(new Point(900, 5, 7), new Point(925, 205, 7)));

        return obstacle;

    }

    @Override
    public Point getStartPoint() {
        return new Point(50, 450, 101);
    }

    @Override
    public Point getEndPoint() {
        return new Point(950, 50, 102);
    }

}
