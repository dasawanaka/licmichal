package michal.daniel.lic.view.data;

import java.util.ArrayList;
import java.util.Random;
import michal.daniel.lic.geometry.Point;
import michal.daniel.lic.geometry.Segment;

/**
 * Klasa służy do generowania losowych danych wykorzystywanych w programie.
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class DataGenerator {

    private int obstacleCount = 0;
    private int maxX = 0;
    private int maxY = 0;
    private static final Random RANDOM = new Random();

    /**
     * Ustawiamy ilość przeszkód które chcemy wygenerować.
     *
     * @param obstacleCount
     */
    public void setObstacleCount(int obstacleCount) {
        this.obstacleCount = obstacleCount;
    }

    /**
     * Ustawiamy szerokość pola rysującego aby przeszkody nie wykraczały.
     *
     * @param maxX Szerokość obszaru roboczego
     */
    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    /**
     * Ustawiamy Wysokość pola rysującego aby przeszkody nie wykraczały.
     *
     * @param maxY Wysokość obszaru roboczego.
     */
    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    /**
     * Funkcja generuje zbiór losowych przeszkód.
     *
     * @return Lista przeszkód.
     */
    public ArrayList<Segment> generateRandomObstacles() {
        ArrayList<Segment> result = new ArrayList<>();
        if (obstacleCount > 0 && maxX > 0 && maxY > 0) {
            for (int obstacleID = 0; obstacleID < obstacleCount; obstacleID++) {
                Point p1 = getRandomPoint(obstacleID);
                Point p2 = getRandomPoint(obstacleID);
                Segment s = new Segment(p1, p2);
                result.add(s);
            }

        }
        return result;
    }

    private Point getRandomPoint(int id) {
        Point p = null;
        if (maxX > 0 && maxY > 0) {
            int x = RANDOM.nextInt(maxX);
            int y = RANDOM.nextInt(maxY);
            p = new Point(x, y, id);
        }
        return p;
    }

    /**
     * Generuje i zwraca model danych zawierający przeszkody oraz punkty startu
     * i końca.
     *
     * @see DataModel
     * @return
     */
    public Data generateDataModel() {
        DataModel dm = new DataModel();
        dm.setObstacles(generateRandomObstacles());
        Point s = getRandomPoint(101);
        Point e = getRandomPoint(102);
        dm.setStarPoint(s);
        dm.setEndPoint(e);
        return dm;
    }

}
